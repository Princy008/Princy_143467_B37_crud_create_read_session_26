<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
//include("Database.php");
class BirthDay extends DB
{
    public $id = "";
    public $user_name= "";
    public $birth_day = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('birth_day',$data))
        {

            $this->birth_day=$data['birth_day'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->birth_day);


        $sql="insert into birthday( user_name,birth_day)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ Username: $this->user_name ] , [ BirthDay: $this->birth_day ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[UserName: $this->user_name] , [BirthDay: $this->birth_day ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }
}