<?php
require_once ("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;


$objBookTitle=new BookTitle();

$allData=$objBookTitle->index("OBJ");
?>


<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <th>Id</th>
        <th>Book Title</th>
        <th>Author name</th>
    </tr>
    <tr>
        <?php foreach ($allData as $oneData){ ?>

        <td><?php echo $oneData->id."<br>" ?></td>
        <td><?php echo $oneData->book_title."<br>" ?></td>
        <td><?php echo $oneData->author_name."<br>"  ?></td>
    </tr>
   <?php } ?>
</table>

</body>
</html>